<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para para servidor pesquisado por nome.
 */
class Desconectar__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {

        $data     = [];
        $data = \App\Operacoes::desconectar();
        $response = $data;

        return $response;
    }
}