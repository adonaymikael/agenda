<?php

namespace Core\HTTP;

class Input
{
    /**
     * Responde a requisições.
     */
    public static function args( $context, $default = null )
    {
        $args = Router::get( 'args' );
        return isset( $args[ $context ] )
            ? $args[ $context ]
            : $default;
    }
}