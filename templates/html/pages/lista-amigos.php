<div>
<a class="nav-link" href="/lista-usuarios">
    <button type="button" class="btn btn-primary"  aria-pressed="false" autocomplete="off">Pesquisar amigos</button>
</a>
</div>
<div>
<?php if ($this->data('solicitacoes') > 0) { ?>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Perfil</th>
                <th scope="col">Solicitação</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        foreach ($this->data('usuario') as $usuario) { ?>
                    <tr>
                        <td><?= $usuario['nome'] ?></td>
                        <td><a href="/perfil?id/<?= $usuario['id'] ?>" class="btn btn-info">Perfil</a></td>
                        <td><a href="/aceitar-solicitacao-amigo?id/<?= $usuario['id'] ?>" class="btn btn-success">Aceitar Solicitação</a></td>
                    </tr>
                <?php 
            }
            ?>
        </tbody>
    </table>
        <?php 
    } ?>
</div>
<div>
<?php 
if ($this->data('amigos') > 0) { ?>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Perfil</th>
                <th scope="col">Agenda</th>
                <th scope="col">Desfazer amizade</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($this->data('amigos') as $amigo) { ?>
                <tr>
                    <td><?= $amigo['nome'] ?></td>
                    <td><a href="/perfil?id/<?= $amigo['id'] ?>" class="btn btn-info">Perfil</a></td>
                    <td><a href="/agenda-amigo?id_amigo/<?= $amigo['id'] ?>" class="btn btn-success">Agenda&nbsp;&nbsp;<img src="\css\svg\calendar.svg" alt="some text" width=20 height=25></a></td>
                    <td><a href="/desfazer-amizade?id/<?= $amigo['id'] ?>" class="btn btn-danger">Desfazer amizade</a></td>
                </tr>
            <?php 
        } ?>
        </tbody>
    </table>
    <?php } ?>
</div>