<?php
if (isset($_SESSION['erro'])) {
    $erro = $_SESSION['erro'];
} else {
    $erro = null;
}
if($erro == "cadastro"){ ?>
    <script>
        $(document).ready(function(){
            $("#cadastroModal").modal();
        });
    </script>
<?php  } ?>

<button type="button" class="btn btn-outline-primary btn" aria-pressed="false" autocomplete="off" class="nav-link" data-toggle="modal" data-target="#cadastroModal">
    Cadastrar
</button>


<div class="modal fade" id="cadastroModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cadastro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <?php if ($erro == "cadastro") { ?>
                     <div class="alert alert-danger" role="alert">
                        <b>Email</b> já cadastrado, por favor tente novamente
                     </div>
                     <?php 
                } ?>
                <h3 class="mb-5">Digite os seus dados</h3>

                <form action="/cadastro" method="POST">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="adicionarNome">Nome</label>
                            <input 
                            name="nome" 
                            id="adcNome" 
                            type="text"
                            class="form-control" 
                            placeholder="Digite o seu nome" 
                            required
                            >
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="adicionarSenha">Senha</label>
                            <input 
                            name="senha" 
                            id="adcSenha" 
                            type="password" 
                            class="form-control" 
                            maxlength="16" 
                            placeholder="Digite a sua senha"
                            >
                        </div>
                        <div class="form-group col-md-6">
                            <label for="adicionarEmail">E-mail</label>
                            <input 
                            name="email" 
                            id="adcEmail" 
                            type="email" 
                            class="form-control" 
                            placeholder="Digite o seu email"
                            >
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="input" class="btn btn-primary" >Cadastrar</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>
 