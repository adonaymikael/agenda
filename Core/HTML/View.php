<?php

namespace Core\HTML;

use Core\HTTP\Router;

/**
 * Monta páginas HTML.
 */
class View
{
    /**
     * Armazena o controller que chamou a view.
     */
    protected $controller;

    /**
     * Armazena a rota da requisição.
     */
    protected $route;

    /**
     * Renderiza uma página.
     */
    public function __construct( $controller )
    {
        $this->controller   = $controller;
        $this->route        = Router::get( 'route' );
    }

    /**
     * Renderiza uma página.
     */
    public function render()
    {
        $filepage = DIR_TEMPLATES . '/pages/' . $this->route . '.php';

        if ( !file_exists( $filepage ) )
        {
            echo "Arquivo de página $filepage não encontrado.";
            exit;
        }

        ob_start();
        include $filepage;
        $this->content = ob_get_clean();

        $fileview   = $this->controller->layout
            ? DIR_TEMPLATES . '/layouts/' . $this->controller->layout . '.php'
            : DIR_TEMPLATES . '/pages/' . $this->route . '.php';

        include $fileview;
    }

    /**
     * Renderiza uma página.
     */
    public function content()
    {
        return $this->content;
    }

    /**
     * Renderiza uma página.
     */
    public function data( $context )
    {
        return $this->controller->data[ $context ];
    }
}