DROP DATABASE IF EXISTS `agenda`;
CREATE SCHEMA `agenda` DEFAULT CHARACTER SET utf8 ;
USE `agenda` ;


-- -----------------------------------------------------
-- Table `agenda`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda`.`usuario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(220) NULL,
  `senha` VARCHAR(220) NULL,
  `nome` VARCHAR(220) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agenda`.`eventos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda`.`eventos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(220) NULL,
  `description` VARCHAR(220) NULL,
  `color` VARCHAR(220) NULL,
  `start` DATETIME NULL,
  `end` DATETIME NULL,
  `share` INT NULL DEFAULT 0,
  `usuario_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_eventos_usuario_idx` (`usuario_id` ASC),
  CONSTRAINT `fk_eventos_usuario`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `agenda`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agenda`.`amigos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda`.`amigos` (
  `id_amigos` INT NOT NULL AUTO_INCREMENT,
  `id_de` INT NOT NULL,
  `id_para` INT NOT NULL,
  `status` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_amigos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agenda`.`grupos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda`.`grupos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nomeGrupo` VARCHAR(220) NULL,
  `id_dono` VARCHAR(220) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agenda`.`membros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda`.`membros` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_membro` INT NULL,
  `grupos_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_membros_grupos1_idx` (`grupos_id` ASC),
  CONSTRAINT `fk_membros_grupos1`
    FOREIGN KEY (`grupos_id`)
    REFERENCES `agenda`.`grupos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agenda`.`convite_grupo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda`.`convite_grupo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `grupos_id` INT NOT NULL,
  `id_de` INT NULL,
  `id_para` INT NULL,
  `status` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_convite_grupo_grupos1_idx` (`grupos_id` ASC),
  CONSTRAINT `fk_convite_grupo_grupos1`
    FOREIGN KEY (`grupos_id`)
    REFERENCES `agenda`.`grupos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agenda`.`eventos_grupo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda`.`eventos_grupo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(220) NULL,
  `description` VARCHAR(220) NULL,
  `color` VARCHAR(220) NULL,
  `start` DATETIME NULL,
  `end` DATETIME NULL,
  `grupos_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_eventos_grupo_grupos1_idx` (`grupos_id` ASC),
  CONSTRAINT `fk_eventos_grupo_grupos1`
    FOREIGN KEY (`grupos_id`)
    REFERENCES `agenda`.`grupos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `agenda`.`solicitacoes_eventos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agenda`.`solicitacoes_eventos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_de` INT NOT NULL,
  `id_para` INT NOT NULL,
  `titulo` VARCHAR(220) NULL,
  `descricao` VARCHAR(220) NULL,
  `inicio` DATETIME NULL,
  `fim` DATETIME NULL,
  PRIMARY KEY (`id`)
);


INSERT INTO `agenda`.`usuario` (`email`, `senha`, `nome`) VALUES ('lol@gmail.com', '123', 'lol');
INSERT INTO `agenda`.`usuario` (`email`, `senha`, `nome`) VALUES ('joj@gmail.com', '123', 'joj');
INSERT INTO `agenda`.`usuario` (`email`, `senha`, `nome`) VALUES ('yoy@gmail.com', '123', 'yoy');
INSERT INTO `agenda`.`usuario` (`email`, `senha`, `nome`) VALUES ('pedrin@gmail.com', '123', 'pedrin');

INSERT INTO `agenda`.`amigos`(`id_amigos`,`id_de`,`id_para`,`status`)VALUES(`1`,`1`,`2`,`1`);
INSERT INTO `agenda`.`amigos`(`id_amigos`,`id_de`,`id_para`,`status`)VALUES(`2`,`1`,`3`,`1`);
INSERT INTO `agenda`.`amigos`(`id_amigos`,`id_de`,`id_para`,`status`)VALUES(`3`,`1`,`4`,`1`);
