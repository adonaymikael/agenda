<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Aceitar_Solicitacao_Amigo__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        $id_de   = Input::args('id');
        $id_para = $_SESSION['id'];
        \App\Operacoes::aceita_solicitacao($id_de, $id_para);
        header('location:lista-amigos');
        
    }
}