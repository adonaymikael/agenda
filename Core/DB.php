<?php

namespace Core;

class DB
{
    /**
     * Conexão ativa.
     */
    private static $conn = null;

    /**
     * Retorna a conexão do banco.
     */
    public static function getConnection()
    {
        if ( !self::$conn )
        {
            self::setConnection();
        }

        return self::$conn;
    }
    
    /**
     * Estabelece a conexão.
     */
    private static function setConnection()
    {
        $config = Config::get( 'db' );
        $dsn = "mysql:host={$config->server};dbname={$config->dbname};charset=utf8";
        self::$conn = new \PDO( $dsn, $config->user, $config->password );
    }
}