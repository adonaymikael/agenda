<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Carrega_Membros__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {

        $id = $_SESSION['id'];
        $idGrupo = Input::args('idGrupo');
        $id = $_SESSION['id'];
        header('location:grupos?id='.$id."&grupo_id=".$idGrupo);
        $this->set( 'listarMembros', \App\Operacoes::lista_membros($idGrupo));

        
    }
}