<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Editar_Evento_Grupo__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            \App\Operacoes::Editar_Evento_Grupo();
            header('location: agenda-grupo?idGrupo/'.$_SESSION['idGrupo']);
            exit();
        }
         

        
    }
}