<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class CriarGrupo__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            \App\Operacoes::Criar_Grupo();
            header('location: /grupos');
            exit();
        }

    }
}