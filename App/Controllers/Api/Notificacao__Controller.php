<?php

namespace App\Controllers\Api;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Notificacao__Controller extends Controller
{
    /**
     * Implementa o serviço.a
     */
    public function execute()
    {
        $this->format = 'json';
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $requestPost = [];
            $requestPost = json_decode(file_get_contents('php://input'), true);
            \App\Operacoes::updateEventColor($requestPost);
            return "sucess";
        }else{
            $data = [];
            $data = \App\Operacoes::getAllEvents();
            $response = $data;
            return $response;
        }
    }
}