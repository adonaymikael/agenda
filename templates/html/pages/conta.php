<h1>Alterar dados da conta<h1>
<div class="form-group">

<form action="/conta" method='POST'>
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="adicionarNome">Nome</label>
            <input 
                name="nome-editar" 
                id="adcNome" 
                type="text"
                class="form-control" 
                placeholder="Digite o seu nome" 
                required
                >
        </div>
    </div>

    <div class="form-row">
    <div class="form-group col-md-8">
        <label for="adicionarEmail">E-mail</label>
            <input 
                name="email-editar" 
                id="adcEmail" 
                type="email" 
                class="form-control" 
                placeholder="Digite o seu email"
                >
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="adicionarSenha">Senha</label>
            <input 
                name="senha-editar" 
                id="adcSenha" 
                type="password" 
                class="form-control" 
                maxlength="16" 
                placeholder="Digite a sua senha"
                >
        </div>
    </div>
    
    <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="input" class="btn btn-primary" >Salvar</button>
    </div>
</form>    