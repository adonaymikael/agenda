<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Lista_Amigos__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        $id_para = Input::args('id');

        if ( $_SESSION['id'] != $id_para ) {
            header("location:lista-amigos?id/" . $_SESSION['id']);
            exit();
        }

        $this->set('solicitacoes', \App\Operacoes::verifica_se_tem_solicitacao($id_para));

        if ( \App\Operacoes::verifica_se_tem_solicitacao( $id_para ) > 0 ) {
            $teste = \App\Operacoes::verifica_se_tem_solicitacao($id_para);
            $this->set('usuario', \App\Operacoes::dados_perfil($teste[0]['id_de']));
        }

        $lista_amigos = \App\Operacoes::lista_amigos( $id_para );
        
        $this->set( 'amigos', $lista_amigos );
    }
}