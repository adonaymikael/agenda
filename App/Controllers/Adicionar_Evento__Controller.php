<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Adicionar_Evento__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        //CHAMAR A OPERAÇÃO 
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            \App\Operacoes::Adicionar_Evento();
            header('location: /calendario');
            exit();
        }

        
    }
}