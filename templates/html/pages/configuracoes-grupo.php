<?php
$membros = $this->data('listarMembros');
?>
<div class="modal fade" id="configuracoes-grupo" tabindex="-1" role="dialog" aria-labelledby="configuracoes-grupo" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Configurações</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form action="/configuracoes-grupo" method="POST">

               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label for="nomeGrupo">Nome do Grupo</label>
                     <?php if ($this->data('listarMembros') > 0) { ?>
                         <input name="nomegrupo" id="nomeGrupo" type="text" class="form-control" value="<?= $this->data('listarMembros')[0]['nomeGrupo']  ?>" required> 
                     <?php } ?>
                  </div>
               </div>
               <br>
					<div>
						<label for="">Membros do grupo</label>
						<table class="table">
							<thead>
								<tr>
									<th scope="col">Nome</th>
									<th scope="col">Perfil</th>
									<th scope="col">Excluir</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ($this->data('listarMembros')) {

									foreach ($this->data('listarMembros') as $membro) { ?>
										<tr>
											<td><?= $membro['nome'] ?></td>
											<td><a href="/perfil?id/<?= $membro['id_membro'] ?>" class="btn btn-info">Perfil</a></td>
											<td><a href="/excluir-do-grupo?id/<?= $membro['id_membro']?>/id-grupo/<?=$membro['grupos_id']?>" class="btn btn-danger">Excluir</a></td>
										</tr>
									<?php }
							} ?>
							</tbody>
						</table>
					</div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                  <button type="input" class="btn btn-primary">Salvar Alterações</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>