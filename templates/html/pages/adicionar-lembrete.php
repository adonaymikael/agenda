<button type="button" class="btn btn-primary" aria-pressed="false" autocomplete="off" class="nav-link" data-toggle="modal" data-target="#solicitacaoModal">
    Adicionar Lembrete
</button>

<div class="modal fade" id="solicitacaoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adicionar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="mb-5">Adicionar os dados</h3>
                
                <form action="/agenda-amigo?id_amigo/<?=$usuario['id']?>" method="POST">
                    <div class="form-row">

                        <div class="form-group col-md-6">
                            <label>Nome do Lembrete</label>
                            <input type="text" class="form-control" name="titulo" id="nome-lembrete" placeholder="nome do evento">
                        </div>

                        <div class="form-group col-md-6">
                            <label>Data do Inicio</label>
                            <input type="datetime-local" class="form-control" name="data-inicio" id="data-inicio" value="startEvent" placeholder="data">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Data do Fim</label>
                            <input type="datetime-local" class="form-control" name="data-fim" id="data-fim" value="endEvent" placeholder="data">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="">Descrição</label>
                            <input type="text" class="form-control" name="desc-lembrete" id="desc-lembrete" placeholder="Descricao do lembrete">
                        </div>

                    </div>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                </form>
            </div>
        </div>
    </div>
</div>
