<?php

namespace Core\HTTP;

/**
 * Classe para capturar a rota das requisições.
 */
class Router
{
    /**
     * Armazena os dados da rota da requisição.
     */
    protected static $route = [];

    /**
     * Captura a rota da requisição.
     */
    public static function getRoute()
    {
        $uri        = preg_replace( '@^\/|\?.*@', '', $_SERVER[ 'REQUEST_URI' ] );
        $args_url   = !empty( $_SERVER[ 'QUERY_STRING' ] )
            ? explode( '/', $_SERVER[ 'QUERY_STRING' ] )
            : [];
        $args       = [];
        $tot_args   = count( $args_url );

        for ( $i = 0; $i < $tot_args; $i += 2 )
        {
            $key            = $args_url[ $i ];
            $value          = isset( $args_url[ $i + 1 ] )
                ? $args_url[ $i + 1 ]
                : null;
            $args[ $key ]   = $value;
        }

        self::$route = [
            'route' => $uri,
            'args'  => $args,
        ];
    }

    /**
     * Retorna os dados da rota.
     */
    public static function get( $context = null )
    {
        if ( !$context )
        {
            return self::$route;
        }

        return isset( self::$route[ $context ] )
            ? self::$route[ $context ]
            : null;
    }
}
