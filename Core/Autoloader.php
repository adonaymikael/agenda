<?php

namespace Core;

class Autoloader
{
    /**
     * Registra o autoload.
     */
    public static function register()
    {
        spl_autoload_register( [__CLASS__, 'autoload'] );
    }

    /**
     * Executa o autoload.
     */
    private static function autoload( $class )
    {
        $path       = dirname( __DIR__ );
        $filename   = str_replace( '\\', '/', $path . '/' . $class . '.php' );
        if ( file_exists( $filename ) )
        {
            include $filename;
        }
    }
}