<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para para servidor pesquisado por nome.
 */
class Dados_Eventos__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {

        $this->format = 'json';

        $data     = [];
        $id   = $_SESSION['id'];
        $data = \App\Operacoes::eventos($id);
        return $data;
    }
}