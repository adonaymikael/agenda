var data = new Date();
if (data.getMonth() < 10) {
    mes = "0" + (data.getMonth() + 1);
} else {
    mes = (data.getMonth() + 1);
}
if (data.getDate() < 10) {
    dia = "0" + data.getDate();
} else {
    dia = data.getDate();
}
var dataHoje = data.getFullYear() + "-" + mes + "-" + dia;

document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        locale: 'pt-br',
        plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        defaultDate: dataHoje,
        navLinks: true,
        selectable: true,
        selectMirror: true,
        select: function (day) {
            $('#adicionarmodal #startEvent').val(moment(day.start).format('YYYY-MM-DDTHH:mm'));
            $('#adicionarmodal #endEvent').val(moment(day.end).format('YYYY-MM-DDTHH:mm'));
            $('#adicionarmodal').modal('show');
        },
        eventClick: function (event) {
            $('#editarmodal #eventid').val(event.event.id);
            $('#editarmodal #tituloEvent').val(event.event.title);
            $('#editarmodal #descricaoEvent').val(event.event.extendedProps.description);
            $('#editarmodal #corEvent').val(event.event.backgroundColor);
            $('#editarmodal #startEvent').val(moment(event.event.start).format('YYYY-MM-DDTHH:mm'));
            $('#editarmodal #endEvent').val(moment(event.event.end).format('YYYY-MM-DDTHH:mm'));
            $('#editarmodal').modal('show');
        },
        eventRender: function (info) {
            var tooltip = new Tooltip(info.el, {
                title: info.event.extendedProps.description,
                placement: 'top',
                trigger: 'hover',
                container: 'body'
            });
        },
        events: "/dados-eventos",
        editable: true,
        eventLimit: true,
        eventDrop: function (info) {
            $('#editarmodal #eventid').val(info.event.id);
            $('#editarmodal #tituloEvent').val(info.event.title);
            $('#editarmodal #descricaoEvent').val(info.event.extendedProps.description);
            $('#editarmodal #corEvent').val(info.event.backgroundColor);
            $('#editarmodal #startEvent').val(moment(info.event.start).format('YYYY-MM-DDTHH:mm'));
            $('#editarmodal #endEvent').val(moment(info.event.end).format('YYYY-MM-DDTHH:mm'));
            $('#btn-editar-evento').click();
        },
    });

    calendar.render();
});
