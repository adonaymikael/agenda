<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Agenda_Amigo__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id_para = Input::args('id_amigo');
            $id_de   = $_SESSION['id'];
            \App\Operacoes::adicionaLembrete($id_de, $id_para);
        }
      
        $idAmigo = Input::args('id_amigo');
        $_SESSION['idAmigo'] = $idAmigo;

        $lista_amigos = \App\Operacoes::lista_amigos( $_SESSION['id'] );

        $this->set( 'amigos', $lista_amigos);
        $this->set( 'usuario', \App\Operacoes::UsuarioByID($idAmigo));  
    }
}