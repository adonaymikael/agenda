<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Calendario__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {   
        $id          = Input::args('id');
        $id_lembrete = Input::args('id_lembrete');
        if(!isset($_SESSION['id'])){
        $idSession   = null;   
        }else{
         $idSession  = $_SESSION['id'];    
        }

        if($idSession == null){
            header("location:landing-page");
            exit();
        }
        $lembretes = \App\Operacoes::lista_lembrete($idSession);
        // echo $id_lembrete; exit();
        $this->set('lembretes', $lembretes);
        $this->set('lembreteModal',\App\Operacoes::seleciona_lembrete($id_lembrete));

        if($idSession == $id){
            $this->set('eventos',  \App\Operacoes::eventos($id));
        }else{
            header("location:calendario?id/".$_SESSION['id']);
            exit();
        }

        
    }
}