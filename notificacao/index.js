const request = require('request');
const mailgun = require('mailgun-js');
const DOMAIN = "sandboxd58740c753544068865f312e75918220.mailgun.org";
const uriHeroku = "https://projetoagenda.herokuapp.com/"; 
const mg = mailgun({apiKey: "258d2fbdc9bad4ee9f5e6369577b24db-52b0ea77-506586fc", domain: DOMAIN});

requestPOST()
requestPOST();
setInterval(function(){
    requestPOST();
}, 7000);

function requestPOST(){
    request.get(uriHeroku+'api/notificacao', function(error, response, body){
        console.log('statusCode:', response && response.statusCode);
        //console.log('body:', body);
        let bodyJson = JSON.parse(body);
        for(var i = 0; i<bodyJson.length; i++){
            let date = new Date();
            let idEvento = null;
            idEvento = bodyJson[i].id
            let options = {
                uri: uriHeroku+'api/notificacao', // atualizar depois p/ URL Heroku.
                method: 'POST',
                json: {
                    "id": idEvento
                }
            }

            let dataInicio = new Date(bodyJson[i].start);
            let dataFim = new Date(bodyJson[i].end);
            let dataAtual = date.customFormat( "#DD#/#MM#/#YYYY# #hh#:#mm#" )
            console.log(`Data Inicio: ${dataInicio.customFormat( "#DD#/#MM#/#YYYY# #hh#:#mm#" )}\nData atual: ${dataAtual}`);
            
            if(dataAtual === dataInicio.customFormat( "#DD#/#MM#/#YYYY# #hh#:#mm#" )){
                //if(dataAtual){
                    console.log('Id do evento ocorrendo', bodyJson[i].id);
                    request(options);
                    console.log('evento atualizado');
                    let evento = {
                        "evento_id": bodyJson[i].id,
                        "dataInicio": dataInicio,
                        "dataFim": dataFim, 
                        "usuario_id": bodyJson[i].usuario_id,
                        "descricao": bodyJson[i].description,
                        "titulo": bodyJson[i].title
                    }
                    console.log("usuario id " + evento.usuario_id); // atualizar depois p/ URL Heroku.
                    request.get(uriHeroku+"api/usuario-id?userid/"+evento.usuario_id, function(error, bodyHTML, response){
                        let userData = JSON.parse(bodyHTML.body);
                        let userInformation = {
                           "email": userData[0].email,
                           "nome": userData[0].nome
                        }
                        console.log('\nInfomações do usuário: ', userInformation);
                        sendEmail(evento, userInformation);
                        setTimeout(() => {
                            console.log('Notificação enviada. \nAguardando 1 minuto\n\n');
                        }, 60000);
                    });      
                    
            }else if(dataAtual > dataFim.customFormat( "#DD#/#MM#/#YYYY# #hh#:#mm#" )){
                updateOldEvent(idEvento);

            }else{
                console.log('Não há eventos no momento.');
            }
        }
    });
}


Date.prototype.customFormat = function(formatString){
	var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhhh,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
	var dateObject = this;
	YY = ((YYYY=dateObject.getFullYear())+"").slice(-2);
	MM = (M=dateObject.getMonth()+1)<10?('0'+M):M;
	MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
	DD = (D=dateObject.getDate())<10?('0'+D):D;
	DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][dateObject.getDay()]).substring(0,3);
	th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
	formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);

	h=(hhh=dateObject.getHours());
	hh = h<10?('0'+h):h;
  hhhh = hhh<10?('0'+hhh):hhh;
	AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
	mm=(m=dateObject.getMinutes())<10?('0'+m):m;
	ss=(s=dateObject.getSeconds())<10?('0'+s):s;
	return formatString.replace("#hhhh#",hhhh).replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
}

function sendEmail(eventosInfo, usuarioInfo){
    const data = {
        from: "Mailgun Sandbox <postmaster@sandboxd58740c753544068865f312e75918220.mailgun.org>",
        //to: usuarioInfo.email,
        to: usuarioInfo.email,
        subject: `Seu evento '${eventosInfo.titulo}' está começando!`,
        text: `Olá, ${usuarioInfo.nome}!\nSó passando pra te lembrar que seu evento '${eventosInfo.titulo}' marcado pra ${eventosInfo.dataInicio} começou agora! \nAbraço!`
    };

    mg.messages().send(data, function (error, body) {
        console.log(body);
    });

}

function updateOldEvent(idEvent){
    let optionsOldEvent = {
        uri: uriHeroku +'api/notificacao', // atualizar depois p/ URL Heroku.
        method: 'POST',
        json: {
            "id": idEvent,
            "status": "expirado"
        }
    }
    request(optionsOldEvent);
    console.log('evento antigo atualizado.');
}

