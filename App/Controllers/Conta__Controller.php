<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Conta__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        //CHAMA A OPERAÇÃO DE EDITAR CADASTRO DE USUÁRIO
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            \App\Operacoes::editar_conta();
            header( 'location: /calendario' );
            exit();
        }
    }
}