<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Agenda_Grupo__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
      
        $idGrupo = Input::args('idGrupo');
        $_SESSION['idGrupo'] = $idGrupo;

        $this->set( 'lista_grupos', \App\Operacoes::lista_grupos($_SESSION['id']));
        $this->set( 'lista_grupos2', \App\Operacoes::lista_grupos2($_SESSION['id']));
        $this->set( 'grupo', \App\Operacoes::GrupoByID($idGrupo));  
    }
}