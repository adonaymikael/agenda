<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Grupos__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        if(!isset($_SESSION['id'])){
        header("location:landing-page");
        exit();     
        }else{
        
        if(isset($_GET['id'])){
            $id_para = $_GET['id'];
        }else{
            header("location:grupos?id=".$_SESSION['id']);
            exit();  
        }

        if($id_para == null ){
            header("location:grupos?id=".$_SESSION['id']);
            exit();    
        }

        if($id_para != $_SESSION['id']){
          header("location:grupos?id=".$_SESSION['id']);  
        }        

        if(isset($_GET['grupo_id'])){
            $idGrupo =$_GET['grupo_id'];
        }else{
            if(isset($_GET['config_id'])){
                $idGrupo =$_GET['config_id'];
            }else{
                $idGrupo =0;
            }
        }

        $lista_amigos = \App\Operacoes::lista_amigos_all($id_para);

        $this->set( 'solicitacao_grupo', \App\Operacoes::aceita_grupo($id_para));
        $this->set( 'lista_grupos', \App\Operacoes::lista_grupos($id_para));
        $this->set( 'lista_grupos2', \App\Operacoes::lista_grupos2($id_para));
        $this->set( 'listarMembros', \App\Operacoes::lista_membros($idGrupo));
        $this->set( 'listarMembrosSolicitacao', \App\Operacoes::lista_membros_solicitacao($idGrupo));
        $this->set( 'amigos', $lista_amigos );

    }

    }
}