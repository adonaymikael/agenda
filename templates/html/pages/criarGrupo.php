<?php
$amigo = $this->data('amigos');
?>
<div class="modal fade" id="criarGrupo" tabindex="-1" role="dialog" aria-labelledby="criarGrupo" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <h5 class="modal-title" id="criarGrupo">Criar Grupo</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
      <div class="modal-body">
         <div class="container">
            <div class="span9 offset1">
               <form name="login" action="/criarGrupo" method="POST">
                  <center><br><br>
                     <h4>Digite o nome do seu grupo</h4>
                     <input required class="form-control" type="text" name="nome_grupo" id="nome_grupo" placeholder="Digite o nome do grupo"><br><br>
                     <?php if($amigo > 0) {?>
                     <h4>Adicione algumas pessoas</h4><br>
                     <?php foreach ($this->data('amigos') as $amigo) { ?>
                        <div class="form-check form-check-inline">
                        <div class="double"><input class="form-check-input" type="checkbox" id="membros" name="membros[]" value='<?=($amigo['id'])?>'></div>
                        <label class="form-check-label" for="inlineCheckbox1"><?=($amigo['nome'])?></label>
                     </div>
                     <?php } ?>
                     <br><br><br> 
                     <?php } ?>
                     <input type="button" class="btn btn-secondary" data-dismiss="modal" value="Sair">&emsp;&emsp;&emsp;<input type="submit"  class="btn btn-primary" value="Criar"><br><br><br>
                  </center>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>  