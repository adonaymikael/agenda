<?php

namespace Core;

class Config
{
    /**
     * Armazena as configurações.
     */
    protected static $config = [];

    /**
     * Retorna as configurações.
     */
    public static function get( $context )
    {
        if ( !isset( self::$config[ $context ] ) )
        {
            self::load( $context );
        }

        return self::$config[ $context ];
    }

    /**
     * Carrega as configurações do arquivo.
     */
    protected static function load( $context )
    {
        $file   = DIR_BASE . '/config/' . $context . '.php';
        $config = file_exists( $file )
            ? include $file
            : null;
        self::$config[ $context ] = json_decode( json_encode( $config ) );
    }
}