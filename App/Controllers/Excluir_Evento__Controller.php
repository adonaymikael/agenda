<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Excluir_Evento__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        //CHAMAR A OPERAÇÃO 
            \App\Operacoes::Excluir_Evento();
            header('location: /calendario');
            exit();

        
    }
}