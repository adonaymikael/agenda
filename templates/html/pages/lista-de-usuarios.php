<div>
    <?php if ($this->data('usuarios') != null) { ?>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Adicionar</th>
            </tr>
        </thead>
        <tbody>
                <?php foreach ($this->data('usuarios') as $usuario) { ?>

                    <tr>
                        <td><?= $usuario['nome'] ?></td>
                        <td><a href="/perfil?id/<?= $usuario['id'] ?>" class="btn btn-info">Perfil</a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } else { ?>
        <h4>Nenhum Usuário Encontrado com esse nome</h4>
    <?php } ?>
</div>