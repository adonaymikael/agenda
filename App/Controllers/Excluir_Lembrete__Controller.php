<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor
 */
class Excluir_Lembrete__Controller extends Controller
{
    /**
     * Implementa o serviço
     */
    public function execute()
    {
        $id_lembrete = Input::args('id_lembrete');
        \App\Operacoes::excluir_lembrete($id_lembrete);
        header('location:/calendario');
    }
}