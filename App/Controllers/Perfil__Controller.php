<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Perfil__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        $id_para = Input::args('id');
        // echo $id_para; exit();
        $this->set('perfil',\App\Operacoes::dados_perfil($id_para));
        $this->set('verifica',\App\Operacoes::verifica_amizade($_SESSION['id'],$id_para));
        
    }
}