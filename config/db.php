<?php

$server = getenv("DB_SERVER");
$user = getenv("DB_USER");
$password = getenv("DB_PASS");
$dbname = getenv("DB_NAME");

if (empty($server)) {
    $server = 'localhost';
}
if (empty($user)) {
    $user = 'root';
}
if (empty($password)) {
    $password = 'uniceub';
}
if (empty($dbname)) {
    $dbname = 'agenda';
}

return [
    'debug'     => true,
    'server'    => $server,
    'user'      => $user,
    'password'  => $password,
    'dbname'    => $dbname,
];