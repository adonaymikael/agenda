<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Agenda</title>
      <!-- Bootstrap CSS CDN -->
      <script src='/resources/moment/moment.js'></script>
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <link rel="stylesheet" href="/css/bootstrap/bootstrap.css">
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <!-- Our Custom CSS -->
      <link rel="stylesheet" href="/css/style.css">
      <!-- Font Awesome JS -->
      <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
      <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
      <!-- Full Calendar -->
      <link href='/resources/fullcalendar/packages/core/main.css' rel='stylesheet' />
      <link href='/resources/fullcalendar/packages/daygrid/main.css' rel='stylesheet' />
      <script src='/resources/fullcalendar/packages/core/main.js'></script>
      <script src='/resources/fullcalendar/packages/daygrid/main.js'></script>
      <script src='/resources/fullcalendar/packages/core/locales/pt-br.js'></script>
      <script src='/resources/fullcalendar/packages/list/main.min.js'></script>
      <script src='/resources/fullcalendar/packages/timegrid/main.min.js'></script>
      <script src='/resources/fullcalendar/packages/moment/main.min.js'></script>
      <script src='/resources/fullcalendar/packages/interaction/main.min.js'></script>
      <!--Include jQuery-->
      <script type="text/javascript" src="/jquery/jquery.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
      <!--Arquivo com as configurações do Caledario-->
      <script src='js/calendario.js'></script>
      <script type="text/javascript">
         function desconnect() {
             window.location.href = "/desconectar";
         }
      </script>
   </head>
   <?php
      $url = $_SERVER['REQUEST_URI'];
      if (!strstr($url, "/login")) {
          ?>
   <body>
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
         <div class="container-fluid">
            <button type="button" id="sidebarCollapse" class="btn btn-info">
            <img src="\css\svg\three-bars.svg" alt="some text" width=60 height=40>
            </button>
            <?php if (isset($_SESSION['nome'])) { ?>
            &emsp;<b>Bem vindo:</b>&nbsp; <?php echo ($_SESSION['nome']); ?>
            <?php 
               } else {
               } ?>
            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-align-justify"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="nav navbar-nav ml-auto">
                  <?php if (isset($_SESSION['id'])) { ?>
                  <li class="nav-item active">
                     <a class="nav-link" href="/perfil?id/<?= $_SESSION['id'] ?>"><button type="button" class="btn btn-outline-primary btn"  aria-pressed="false" autocomplete="off">Conta</button> </a>
                  </li>
                  <?php 
                     } else { ?>
                  <li class="nav-item active">
                     <button type="button" class="btn btn-outline-primary btn"  aria-pressed="false" autocomplete="off" data-toggle="modal" data-target="#loginmodal">Entrar</button>
                  </li>
                  &emsp;
                  <!-- ADICIONA O BOTÃO DE CADASTRO -->
                  <li class="nav-item active">
                     <?php 
                        include "../templates/html/pages/cadastro.php";
                        ?>
                  </li>
                  <?php 
                     } ?>
               </ul>
            </div>
         </div>
      </nav>
      <div class="wrapper">
         <nav id="sidebar">
            <div class="sidebar-header">
               <h3> </h3>
            </div>
            
            <ul class="list-unstyled components">
               <li>
                  <a href="/calendario">Agenda</a>
               </li>
               <li>
                <?php if(isset($_SESSION['id'])) { ?>
                  <a href="/lista-amigos?id/<?=$_SESSION['id']?>">Amigos</a>
                  <?php } else { ?>
                  <a href="#">Amigos</a>
                  <?php } ?>
               </li>
               <li>
                  <a href="/grupos">Grupos</a>
               </li>
               <?php if(isset($_SESSION['id'])) { ?>
               <li>
                  <a name="desconectar" value="Desconectar" onclick="desconnect();"> Desconectar </a>
               </li>
               <li>
               <br>
               <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-text="Estou usando #agendaApp! Recomendo demais pra quem quer organizar os estudos!" data-url="https://projetoagenda.herokuapp.com/" data-hashtags="teamAgenda" data-lang="pt" data-show-count="false">Tweetar</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
               </li>
               <?php }else{} ?>
            </ul>
         </nav>
         <!-- Incluir Todas as paginas -->
         
         <!-- Page Content  -->
         <div id="content" class="col-10">
            <!-- O CONTEÚDO VEM AQUI -->
            <?= $this->content ?>
         </div>
      </div>
      <?php } else { ?>
      <body>
         <?= $this->content ?>
         <?php 
            } ?>
         <!-- Bootstrap JS -->
         <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
         <script type="text/javascript">
            $(document).ready(function() {
                $('#sidebarCollapse').on('click', function() {
                    $('#sidebar').toggleClass('active');
                });
            });
         </script>
         <script>
            $('#loginmodal').on('hidden.bs.modal', function (e) {
             <?php unset($_SESSION['erro']); ?>
            })
         </script> 
         <script>
            $('#cadastroModal').on('hidden.bs.modal', function (e) {
             <?php unset($_SESSION['erro']); ?>
            })
             
         </script> 
   </body>
</html>
<?php
   include "../templates/html/pages/login.php";
   ?>