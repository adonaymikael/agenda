<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Cadastro__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        //CHAMA A OPERAÇÃO DE CADASTRAR UM USUÁRIO
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            \App\Operacoes::cadastro();
            header( 'location: /calendario' );
            exit();
        }
    }
}