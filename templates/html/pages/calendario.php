<?php
include '../templates/html/pages/editar-evento.php';
include '../templates/html/pages/adicionar-evento.php';
include '../templates/html/pages/adicionar-evento-lembrete.php';
include "../templates/html/pages/login.php";

if (isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
} else {
    $id = null;
}
if (isset($_SESSION['login'])) {
    $login = $_SESSION['login'];
} else {
    $login = null;
}
if (isset($_SESSION['email'])) {
    $email = $_SESSION['email'];
} else {
    $email = null;
}
if (isset($_SESSION['nome'])) {
    $nome = $_SESSION['nome'];
} else {
    $nome = null;
}
if (isset($_SESSION['erro'])) {
    $erro = $_SESSION['erro'];
} else {
    $erro = null;
}

if($this->data('lembretes') > 0){
    foreach ($this->data('lembretes') as $lembrete) {
        ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Um lembrete de <?= $lembrete['nome'] ?>: <?= $lembrete['titulo'] ?></strong>
            Você deseja adicionar ele a sua agenda? <a href="/calendario?id/<?=$_SESSION['id']?>/id_lembrete/<?=$lembrete['id']?>" class="btn btn-info">Clique aqui!</a>
            <a href="/excluir-lembrete?id_lembrete/<?=$lembrete['id']?>" type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </a>
        </div>
    <?php }
} ?>
<div id="content">
    <div id='calendar'></div><br>
    <script src='https://unpkg.com/popper.js/dist/umd/popper.min.js'></script>
    <script src='https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js'></script>


    <?php foreach ($this->data('eventos') as $evento) { ?>
        <div class="alert alert-primary" role="alert">
            <b><?php echo $evento['data']; ?> >>> </b><?php echo $evento['title']; ?> às <?php echo $evento['horario']; ?>
        </div>
    <?php } ?>
</div>
<script type="text/javascript">
	var url = window.location.href;

	if (url.match(/id_lembrete/)) {
		$(document).ready(function() {
			$('#lembreteModal').modal('show');
		});
	}
</script>