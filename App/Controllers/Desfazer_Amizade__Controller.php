<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Desfazer_Amizade__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        $id = Input::args('id');
        \App\Operacoes::cancela_solicitacao($id, $_SESSION['id']);
        header('location:lista-amigos?id/'.$_SESSION['id']);
        exit();
    }
}