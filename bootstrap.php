<?php

session_start();

define( 'DIR_BASE', str_replace( '\\', '/', __DIR__ ) );
define( 'DIR_TEMPLATES', DIR_BASE . '/templates/html' );

include __DIR__ . '/Core/Autoloader.php';
require __DIR__ . '/Core/error-handler.php';
require __DIR__ . '/Core/resolve-route.php';


Core\Autoloader::register();
Core\HTTP\Router::getRoute();
Core\HTTP\Controller::respond();