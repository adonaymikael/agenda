<?php

namespace Core\HTTP;

use Core\HTML\View;

class Controller
{
    /**
     * Armazena o nome do controlador.
     */
    public static $controllerName;
    
    /**
     * Formato da resposta.
     */
    public $layout = 'site';
    
    /**
     * Formato da resposta.
     */
    public $format = 'html';
    
    /**
     * Dados do controlador.
     */
    public $data = [];
    
    /**
     * Dados do controlador.
     */
    protected static $validFormats = [
        'html',
        'json',
        'text',
        'php',
    ];
    
    /**
     * Define dados para o controlador.
     */
    public function set( $context, $value )
    {
        $this->data[ $context ] = $value;
    }

    /**
     * Responde a requisições.
     */
    public static function respond()
    {
        self::defineController();
        self::validateController();
        self::executeController();
    }

    /**
     * Responde a requisições.
     */
    public static function defineController()
    {
        $route                  = Router::get( 'route' );
        $controller             = ucfirst( $route );
        $controller             = str_replace( '/', '\\', $controller );
        $controller             = str_replace( '-', '_', $controller );
        $controller             = preg_replace_callback( '@(\\\.|_.)@', function( $matches ) {
            return strtoupper( $matches[1] );
        }, $controller );
        self::$controllerName   = 'App\\Controllers\\' . $controller . '__Controller';
    }

    /**
     * Responde a requisições.
     */
    public static function validateController()
    {
        $controller = self::$controllerName;

        if ( !class_exists( $controller ) )
        {
            echo "Classe $controller não encontrada.";
            exit;
        }

        if ( !is_subclass_of( $controller, __CLASS__ ) )
        {
            $core_control = __CLASS__;
            echo "Classe $controller não extende à classe $core_control.";
            exit;
        }
    }

    /**
     * Responde a requisições.
     */
    public static function executeController()
    {
        $controller = new self::$controllerName();
        $dados      = $controller->execute();

        if ( !in_array( $controller->format, self::$validFormats ) )
        {
            echo "Formato '$controller->format' desconhecido.";
            exit;
        }

        if ( $controller->format == 'html' )
        {
            $view  = new View( $controller );
            $view->render();
            exit();
        }
        
        $method  = 'sendResponse' . ucfirst( $controller->format );
        self::$method( $dados );
    }
    
    /**
     * Envia a resposta em JSON.
     */
    protected static function sendResponseJson( $dados )
    {
        header( 'Content-Type: application/json' );
        echo json_encode( $dados );
    }
    
    /**
     * Envia a resposta em PHP.
     */
    protected static function sendResponsePhp( $dados )
    {
        header( 'Content-Type: text/php' );
        echo serialize( $dados );
    }
    
    /**
     * Envia a resposta em TXT.
     */
    protected static function sendResponseTxt( $dados )
    {
        header( 'Content-Type: text/plain' );
        print_r( $dados );
    }
}
