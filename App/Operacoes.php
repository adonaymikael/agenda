<?php

namespace App;

class Operacoes
{

    public static function AllByID($id)
    {
        $pdo = \Core\DB::getConnection();
        $sql = "SELECT * from eventos";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    }

    public static function eventos()
    {
        if (isset($_SESSION['id'])) {
            $id = $_SESSION['id'];
        } else {
            $id = null;
        }

        $pdo = \Core\DB::getConnection();
        $sql = "SELECT * from eventos WHERE usuario_id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $resultado = [];
        foreach ($rows as $i => $row) {
            $resultado[$i]['id'] = (int)$row['id'];
            $resultado[$i]['title'] = $row['title'];
            $resultado[$i]['color'] = $row['color'];
            $resultado[$i]['description'] = $row['description'];
            $resultado[$i]['share'] = $row['share'];
            $resultado[$i]['start'] = date("Y-m-d\TH:i:s", strtotime($row['start']));
            $resultado[$i]['end'] = date("Y-m-d\TH:i:s", strtotime($row['end']));

            $resultado[$i]['data'] = date("d/m/Y", strtotime($row['start']));
            $resultado[$i]['horario'] = date("H:i", strtotime($row['start']));

        }
        return $resultado;
    }

    public static function eventosAmigos() {

        if (isset($_SESSION['idAmigo'])) {
            $id =$_SESSION['idAmigo'];
        } else {
            $id = null;
        }


        $pdo = \Core\DB::getConnection();
        $sql = 'SELECT * from eventos WHERE usuario_id = ? AND share = 1';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $resultado = [];
        foreach ($rows as $i => $row) {
            $resultado[$i]['id'] = (int)$row['id'];
            $resultado[$i]['title'] = $row['title'];
            $resultado[$i]['color'] = $row['color'];
            $resultado[$i]['description'] = $row['description'];
            $resultado[$i]['share'] = $row['share'];
            $resultado[$i]['start'] = date("Y-m-d\TH:i:s", strtotime($row['start']));
            $resultado[$i]['end'] = date("Y-m-d\TH:i:s", strtotime($row['end']));

            $resultado[$i]['data'] = date("d/m/Y", strtotime($row['start']));
            $resultado[$i]['horario'] = date("H:i", strtotime($row['start']));
        }
        return $resultado;
    }

    public static function eventosGrupos() {

         if (isset($_SESSION['idGrupo'])) {
             $id =$_SESSION['idGrupo'];
         } else {
             $id = null;
         }

        $pdo = \Core\DB::getConnection();
        $sql = "SELECT * from eventos_grupo where grupos_id =?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $resultado = [];
        foreach ($rows as $i => $row) {
            $resultado[$i]['id'] = (int)$row['id'];
            $resultado[$i]['title'] = $row['title'];
            $resultado[$i]['color'] = $row['color'];
            $resultado[$i]['description'] = $row['description'];
            $resultado[$i]['start'] = date("Y-m-d\TH:i:s", strtotime($row['start']));
            $resultado[$i]['end'] = date("Y-m-d\TH:i:s", strtotime($row['end']));

            $resultado[$i]['data'] = date("d/m/Y", strtotime($row['start']));
            $resultado[$i]['horario'] = date("H:i", strtotime($row['start']));

        }
        return $resultado;
    }



    public static function login()
    {
        $email = filter_input(INPUT_POST, 'email');
        $senha = filter_input(INPUT_POST, 'senha');
        $id = null;

        $pdo = \Core\DB::getConnection();
        $sql = 'SELECT * FROM usuario WHERE email = ? AND senha= ?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $email);
        $stmt->bindValue(2, $senha);
        $stmt->execute();
        $array = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        if (sizeof($array) > 0) {
            $id = $array[0]['id'];
            $email = $array[0]['email'];
            $nome = $array[0]['nome'];
        }

        if ($id > 0) {
            $_SESSION['id'] = $id;
            $_SESSION['senha'] = $senha;
            $_SESSION['nome'] = $nome;
            $_SESSION['email'] = $email;
            header('location:calendario');
        } else {
            $_SESSION['erro'] = "login";
            header('location:landing-page');
            exit();
        }
    }
    public static function desconectar()
    {
        unset($_SESSION['login']);
        unset($_SESSION['id']);
        unset($_SESSION['email']);
        unset($_SESSION['senha']);
        unset($_SESSION['nome']);
        header("location:landing-page");
    }

    public static function Adicionar_Evento()
    {
        $id = $_SESSION['id'];
        $pdo = \Core\DB::getConnection();
        $title = filter_input(INPUT_POST, 'title');
        $DataInicio = filter_input(INPUT_POST, 'data-inicio');
        $DataFim = filter_input(INPUT_POST, 'data-fim');
        $cor = filter_input(INPUT_POST, 'cor');
        $descricao = filter_input(INPUT_POST, 'desc-event');
        $share = filter_input(INPUT_POST, 'share');

        
        $sql = 'INSERT INTO  eventos (title,start,end,color,description,share,usuario_id) values(?,?,?,?,?,?,?)';

        $stmt = $pdo->prepare($sql);

        $stmt->bindValue(1, $title);
        $stmt->bindValue(2, $DataInicio);
        $stmt->bindValue(3, $DataFim);
        $stmt->bindValue(4, $cor);
        $stmt->bindValue(5, $descricao);
        $stmt->bindValue(6, $share);
        $stmt->bindValue(7, $id);
        return $stmt->execute();
    }

    public static function Adicionar_Evento_Grupo()
    {
        $id = $_SESSION['idGrupo'];
        $pdo = \Core\DB::getConnection();
        $title = filter_input(INPUT_POST, 'title');
        $DataInicio = filter_input(INPUT_POST, 'data-inicio');
        $DataFim = filter_input(INPUT_POST, 'data-fim');
        $cor = filter_input(INPUT_POST, 'cor');
        $descricao = filter_input(INPUT_POST, 'desc-event');
        
        $sql = 'INSERT INTO  eventos_grupo (title,start,end,color,description,grupos_id) values(?,?,?,?,?,?)';

        $stmt = $pdo->prepare($sql);

        $stmt->bindValue(1, $title);
        $stmt->bindValue(2, $DataInicio);
        $stmt->bindValue(3, $DataFim);
        $stmt->bindValue(4, $cor);
        $stmt->bindValue(5, $descricao);
        $stmt->bindValue(6, $id);
        return $stmt->execute();
    }

    public static function Editar_Evento()
    {
        $id = $_SESSION['id'];
        $pdo = \Core\DB::getConnection();
        $idevent = filter_input(INPUT_POST, 'eventid');
        $title = filter_input(INPUT_POST, 'title-editar');
        $color = filter_input(INPUT_POST, 'cor-editar');
        $descricao = filter_input(INPUT_POST, 'desc-event');
        $share = filter_input(INPUT_POST, 'share');

        $DataInicio = filter_input(INPUT_POST, 'data-inicio-editar');
        $DataFim = filter_input(INPUT_POST, 'data-fim-editar');
        $sql = 'UPDATE eventos SET title=? ,color=?, description=?,start=?, end=?, share=? WHERE id = ?  ';

        $stmt = $pdo->prepare($sql);

        $stmt->bindValue(1, $title);
        $stmt->bindValue(2, $color);
        $stmt->bindValue(3, $descricao);
        $stmt->bindValue(4, $DataInicio);
        $stmt->bindValue(5, $DataFim);
        $stmt->bindValue(6, $share);
        $stmt->bindValue(7, $idevent);
        return $stmt->execute();
    }

    public static function Editar_Evento_Grupo()
    {
        $id = $_SESSION['idGrupo'];
        $pdo = \Core\DB::getConnection();
        $idevent = filter_input(INPUT_POST, 'eventid');
        $title = filter_input(INPUT_POST, 'title-editar');
        $color = filter_input(INPUT_POST, 'cor-editar');
        $descricao = filter_input(INPUT_POST, 'desc-event');

        $DataInicio = filter_input(INPUT_POST, 'data-inicio-editar');
        $DataFim = filter_input(INPUT_POST, 'data-fim-editar');
        $sql = 'UPDATE eventos_grupo SET title=? ,color=?, description=?,start=?, end=? WHERE id = ?  ';

        $stmt = $pdo->prepare($sql);

        $stmt->bindValue(1, $title);
        $stmt->bindValue(2, $color);
        $stmt->bindValue(3, $descricao);
        $stmt->bindValue(4, $DataInicio);
        $stmt->bindValue(5, $DataFim);
        $stmt->bindValue(6, $idevent);
        return $stmt->execute();
    }



    public static function Excluir_Evento()
    {
       
        $pdo = \Core\DB::getConnection();
        $idevent = filter_input(INPUT_GET, 'eventid');
        $sql = 'DELETE FROM eventos WHERE id = ?';

        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $idevent);
        return $stmt->execute();
    }

    public static function Excluir_Evento_Grupo()
    {
       
        $pdo = \Core\DB::getConnection();
        $idevent = filter_input(INPUT_GET, 'eventid');
        $sql = 'DELETE FROM eventos_grupo WHERE id = ?';

        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $idevent);
        return $stmt->execute();
    }


    public static function cadastro()
    {
        $pdo = \Core\DB::getConnection();
        $senha = filter_input(INPUT_POST, 'senha');
        $nome = filter_input(INPUT_POST, 'nome');
        $email = filter_input(INPUT_POST, 'email');
        $sql = 'SELECT * FROM usuario WHERE email=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $email);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
        if (!sizeof($result) > 0) {
            
            $sql = 'INSERT INTO usuario (senha, nome, email) VALUES (?,?,?)';
    
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(1, $senha);
            $stmt->bindValue(2, $nome);
            $stmt->bindValue(3, $email);
            $result = $stmt->execute();
    
            $sql = 'SELECT * FROM usuario WHERE email = ? AND senha= ?';
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(1, $email);
            $stmt->bindValue(2, $senha);
            $stmt->execute();
            $array = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            if (sizeof($array) > 0) {
                $id = $array[0]['id'];
            }
    
            $_SESSION['id'] = $id;
            $_SESSION['senha'] = $senha;
            $_SESSION['nome'] = $nome;
            $_SESSION['email'] = $email;
            return $result;
        } else {
            $_SESSION['erro'] = "cadastro";
            header('location:landing-page');
        }

    }

    public static function editar_conta(){
        $id = $_SESSION['id'];

        $nome = filter_input(INPUT_POST, 'nome-editar');
        $email = filter_input(INPUT_POST, 'email-editar');
        $senha = filter_input(INPUT_POST, 'senha-editar');

        $pdo = \Core\DB::getConnection();
        $sql = 'UPDATE usuario SET nome=?, email=?, senha=? WHERE id=?';
        $stmt = $pdo->prepare($sql);

        $stmt->bindValue(1, $nome);
        $stmt->bindValue(2, $email);
        $stmt->bindValue(3, $senha);
        $stmt->bindValue(4, $id);

        $_SESSION['senha'] = $senha;
        $_SESSION['nome'] = $nome;
        $_SESSION['email'] = $email;

        return $stmt->execute(); 
    }

    public static function getAllEvents(){
        $pdo = \Core\DB::getConnection();
        $sql = "SELECT * from eventos";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $rows;
    }

    public static function updateEventColor($eventInfo){
        if($eventInfo["status"] == "expirado"){
            $color = "#696969";
        }else{
            $color = "#F26915";
        }
        
        $pdo = \Core\DB::getConnection();
        $sql = "UPDATE eventos SET color=? WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $color);
        $stmt->bindValue(2, $eventInfo["id"]);
        $stmt->execute();
    }
    public static function lista_usuarios(){
        $nome = filter_input(INPUT_POST, 'procurar-nome');
        $nome = $nome."%";
        $pdo  = \Core\DB::getConnection();
        $sql  = 'SELECT * FROM usuario WHERE nome LIKE ?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $nome);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public static function dados_perfil($id){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'SELECT * FROM usuario WHERE id=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    public static function verifica_amizade($id_de, $id_para){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'SELECT * FROM amigos WHERE id_de=? AND id_para=? OR id_para=? AND id_de=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id_de);
        $stmt->bindValue(2, $id_para);
        $stmt->bindValue(3, $id_de);
        $stmt->bindValue(4, $id_para);
        $stmt->execute();
        if ($stmt->rowCount() < 1) {
            return $stmt->rowCount();
        }
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    public static function cancela_solicitacao($id_de, $id_para){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'DELETE FROM amigos WHERE id_de=? AND id_para=? OR id_para=? AND id_de=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id_de);
        $stmt->bindValue(2, $id_para);
        $stmt->bindValue(3, $id_de);
        $stmt->bindValue(4, $id_para);
        $stmt->execute();

    }
    public static function verifica_se_tem_solicitacao($id_para){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'SELECT * FROM amigos WHERE id_para=? AND status = 0';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id_para);
        $stmt->execute();
        if ($stmt->rowCount() < 1) {
            return $stmt->rowCount();
        }
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    public static function envia_solicitacao($id_de, $id_para){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'INSERT INTO amigos (id_de, id_para) VALUES (?,?)';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id_de);
        $stmt->bindValue(2, $id_para);
        $stmt->execute();
    }
    public static function aceita_solicitacao($id_de, $id_para){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'UPDATE amigos SET status = 1 WHERE id_de=? AND id_para=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id_de);
        $stmt->bindValue(2, $id_para);
        $stmt->execute(); 
    }
    
    public static function lista_amigos($id){
        $pdo  = \Core\DB::getConnection();
        $sql  = '
            SELECT  IF ( u1.id = ?, u2.nome, u1.nome ) nome,
                    IF ( u1.id = ?, u2.email, u1.email ) email,
                    IF ( u1.id = ?, u2.id, u1.id ) id,
                    a.id_amigos
            FROM    amigos a
            JOIN    usuario u1
            ON      u1.id = a.id_de
            JOIN    usuario u2
            ON      u2.id = a.id_para
            WHERE   a.status = 1 AND ( id_de = ?  OR id_para = ? )
        ';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->bindValue(2, $id);
        $stmt->bindValue(3, $id);
        $stmt->bindValue(4, $id);
        $stmt->bindValue(5, $id);
        $stmt->execute();
        return $stmt->rowCount()
            ? $stmt->fetchAll(\PDO::FETCH_ASSOC)
            : 0;
    }


    public static function lista_amigos_all($id){
        $pdo  = \Core\DB::getConnection();
        $sql  = '
        SELECT a.id_amigos, IF ( u1.id = ?, u2.id, u1.id ) id, IF ( u1.id = ?, u2.nome, u1.nome ) nome,IF ( u1.id = ?, u2.email, u1.email ) email, u2.senha,id_de, id_para
        FROM    amigos a
       JOIN    usuario u1
        ON      u1.id = a.id_de
       JOIN    usuario u2
        ON      u2.id = a.id_para
        WHERE   a.status = 1 AND ( id_de = ?  OR id_para = ? ) ';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->bindValue(2, $id);
        $stmt->bindValue(3, $id);
        $stmt->bindValue(4, $id);
        $stmt->bindValue(5, $id);
        $stmt->execute();
        
        
        return $stmt->rowCount() ? $stmt->fetchAll(\PDO::FETCH_ASSOC) : 0;
    }

    public static function Criar_Grupo(){
        $idDono = $_SESSION['id'];
        $nomeGrupo=$_POST['nome_grupo'];

        if(isset($_POST['membros'])){
            $membros =$_POST['membros'];
        }else{
            $membros = null;
        }


         $pdo  = \Core\DB::getConnection();
         $sql  = 'INSERT INTO grupos (nomeGrupo,id_dono) VALUES (?,?)';
         $stmt = $pdo->prepare($sql);
         $stmt->bindValue(1, $nomeGrupo);
         $stmt->bindValue(2, $idDono);
         $stmt->execute();


        $sql  = 'SELECT * FROM grupos WHERE id_dono =? and nomeGrupo =?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $idDono);
        $stmt->bindValue(2, $nomeGrupo);
        $stmt->execute();
        $array = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $idGrupo = $array[0]['id'];

         $sql  = 'INSERT INTO membros (id_membro,grupos_id) VALUES (?,?)';
         $stmt = $pdo->prepare($sql);
         $stmt->bindValue(1, $idDono);
         $stmt->bindValue(2, $idGrupo);
         $stmt->execute();

        if(sizeof($membros) > 0){
        foreach($membros as $id_para){
         $sql  = 'INSERT INTO convite_grupo (grupos_id,id_de, id_para) VALUES (?,?,?)';
         $stmt = $pdo->prepare($sql);
         $stmt->bindValue(1, $idGrupo);
         $stmt->bindValue(2, $idDono);
         $stmt->bindValue(3, $id_para);
         $stmt->execute(); 
        }
    }
    
    }

    public static function verifica_se_tem_solicitacao_grupo($id_para){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'SELECT * FROM convite_grupo WHERE id_para=? AND status = 0';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id_para);
        $stmt->execute();
        if ($stmt->rowCount() < 1) {
            return $stmt->rowCount();
        }
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public static function aceita_grupo($id){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'select a.id as id_Tabela_convite,b.id,b.id_dono,b.nomeGrupo,a.grupos_id,a.id_de,a.id_para,a.status From convite_grupo A
        INNER JOIN grupos B ON A.grupos_id = b.id AND a.id_para=? AND status = 0';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->rowCount() ? $stmt->fetchAll(\PDO::FETCH_ASSOC) : 0;
    }

    public static function aceita_solicitacao_grupo($id_de, $id_para, $grupos_id){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'UPDATE convite_grupo SET status = 1 WHERE id_de=? AND id_para=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id_de);
        $stmt->bindValue(2, $id_para);
        $stmt->execute(); 

        $sql  = 'INSERT INTO membros (id_membro,grupos_id) VALUES (?,?)';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id_para);
        $stmt->bindValue(2, $grupos_id);
        $stmt->execute();
    }

    public static function lista_grupos($id){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'select a.id as id_Membros_Grupo,id_membro,grupos_id,b.id,nomeGrupo,id_dono 
        From membros A INNER JOIN grupos B ON a.grupos_id = b.id AND id_membro = ?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public static function lista_grupos2($id){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'select a.id as id_Membros_Grupo,id_membro,grupos_id,b.id,nomeGrupo,id_dono 
        From membros A INNER JOIN grupos B ON a.grupos_id = b.id AND id_membro = ?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public static function lista_membros($idGrupo){
        $pdo  = \Core\DB::getConnection();
        $sql  = '
        select a.id as id_membros_tabela,id_membro,grupos_id,b.id as id_grupo_tabela,nomeGrupo,id_dono,c.id as id_usuario_tabela,nome,email,senha From membros A
        INNER JOIN grupos B ON a.grupos_id = b.id
        INNER JOIN usuario C ON a.id_membro = c.id where b.id = ?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $idGrupo);
        $stmt->execute();

        return $stmt->rowCount()
            ? $stmt->fetchAll(\PDO::FETCH_ASSOC)
            : 0;
    }


    public static function editar_grupo($id){
        $nome = filter_input(INPUT_POST, 'nomegrupo');
        $pdo  = \Core\DB::getConnection();
        $sql  = 'UPDATE grupos SET nomeGrupo = ? WHERE id_dono=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $nome);
        $stmt->bindValue(2, $id);
        $stmt->execute();
    }

    public static function Excluir_Do_Grupo($id, $id_grupo){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'DELETE FROM membros WHERE id_membro=? AND grupos_id=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->bindValue(2, $id_grupo);
        $stmt->execute();
    }
    
    public static function lista_membros_solicitacao($idGrupo){
        $pdo  = \Core\DB::getConnection();
        $sql  = '
        select c.id,IF( a.grupos_id = ?, c.nome, b.nome ) nome,c.email,c.senha, a.id as id_Tabela_Convite_grupo, grupos_id, id_de, id_para,status from convite_grupo A
        INNER JOIN usuario B ON a.id_de = b.id
        INNER JOIN usuario C ON a.id_para = c.id
        where grupos_id = ? AND status = 0';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $idGrupo);
        $stmt->bindValue(2, $idGrupo);
        $stmt->execute();

        return $stmt->rowCount()
            ? $stmt->fetchAll(\PDO::FETCH_ASSOC)
            : 0;
    }

    public static function UsuarioByID($id){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'select * from usuario where id =?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->rowCount() ? $stmt->fetch(\PDO::FETCH_ASSOC) : 0;

    }

    public static function GrupoByID($id){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'select * from grupos where id =?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        return $stmt->rowCount() ? $stmt->fetch(\PDO::FETCH_ASSOC) : 0;

    }

    public static function adicionaLembrete($id_de, $id_para){
        $pdo       = \Core\DB::getConnection();
        $titulo    = filter_input(INPUT_POST, 'titulo');
        $descricao = filter_input(INPUT_POST, 'desc-lembrete');
        $inicio    = filter_input(INPUT_POST, 'data-inicio');
        $fim       = filter_input(INPUT_POST, 'data-fim');

        $sql       = 'INSERT INTO solicitacoes_eventos (id_de, id_para, titulo, descricao, inicio, fim) VALUES (?,?,?,?,?,?)';
        $stmt      = $pdo->prepare($sql);

        $stmt->bindValue(1, $id_de);
        $stmt->bindValue(2, $id_para);
        $stmt->bindValue(3, $titulo);
        $stmt->bindValue(4, $descricao);
        $stmt->bindValue(5, $inicio);
        $stmt->bindValue(6, $fim);
        
        $stmt->execute();
    }

    public static function lista_lembrete($id_para) {
        $pdo  = \Core\DB::getConnection();
        $sql  = 'SELECT a.id, id_de, nome, titulo, descricao, inicio, fim FROM solicitacoes_eventos a INNER JOIN usuario b WHERE id_para=? AND id_de = b.id';  
        
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id_para);
        $stmt->execute();

        return $stmt->rowCount() ? $stmt->fetchAll(\PDO::FETCH_ASSOC) : 0;
    }

    public static function seleciona_lembrete($id_lembrete){
        $pdo  = \Core\DB::getConnection();
        $sql  = 'SELECT * FROM solicitacoes_eventos WHERE id=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id_lembrete);
        $stmt->execute();

        return $stmt->rowCount() ? $stmt->fetch(\PDO::FETCH_ASSOC):0;
    }

    public static function excluir_lembrete($id_lembrete){
        $pdo = \Core\DB::getConnection();
        $sql = 'DELETE FROM solicitacoes_eventos WHERE id=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id_lembrete);
        $stmt->execute();
    }
}