<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Envia_Solicitacao__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        $id_para = Input::args('id');
        \App\Operacoes::envia_solicitacao($_SESSION['id'], $id_para); 
        header("location:perfil?id/".$id_para);
        exit();  
    }
}