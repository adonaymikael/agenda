<div>
    <?php foreach ($this->data('perfil') as $usuario) { ?>
        <h2><?= $usuario['nome'] ?></h2>
    <?php }
    if ($usuario['id'] != $_SESSION['id']) {
        if ($this->data('verifica') == 0) { ?>
            <a href='/envia-solicitacao?id/<?= $usuario['id'] ?>' class='btn btn-success'>Adicionar amigo<a>
        <?php }
        if ($this->data('verifica') > 0) {
            foreach ($this->data('verifica') as $verifica) {
                if ($verifica['status'] == 0) { ?>
                    <a href='/cancela-solicitacao?id/<?= $usuario['id'] ?>' class='btn btn-secondary'>Cancelar solicitação<a>
                    <?php
                } elseif ($verifica['status'] == 1) { ?>
                    <a href='#' class='btn btn-danger'>Teste<a>
                <?php
                }
            }
        }
    } else {?>
        <a href='/conta' class='btn btn-info'>Configurações da conta<a>
    <?php } ?>
</div>