<?php

namespace App\Controllers\Api;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Usuario_Id__Controller extends Controller
{

    public function execute()
    {
        $this->format = 'json';        
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            $user_Id = Input::args('userid');
            $data = [];
            $data = \App\Operacoes::dados_perfil($user_Id);
            $response = $data;
            return $response;
            //exit();
        }else{
            exit();
        }
    }
}