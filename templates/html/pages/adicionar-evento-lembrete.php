

<div class="modal fade" id="lembreteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Lembrete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="mb-5">Adicionar lembrete</h3>

                <form action="/adicionar-evento" method="POST">
                    <div class="form-group ">
                        <table class="tg">
                            <tr>
                                <th class="tg-eniz">Cor:&emsp;<input type="color" class="form-event" name="cor" id="cor" value="corEvent" placeholder="cor">&emsp;&emsp;</th>
                                <th class="tg-s6z2">
                                    <div class="double"><input type="checkbox" value="1" name="share" checked></div>
                                </th>
                                <th class="tg-eniz">Compartilhar com amigos</th>
                            </tr>
                        </table>
                    </div>


                    <?php if($this->data('lembreteModal') > 0){?>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" name="eventid" id="eventid" value="eventid" hidden>
                            <label>Nome do Evento</label>
                            <input type="text" class="form-control" name="title" id="title" value="<?= $this->data('lembreteModal')['titulo'] ?>" placeholder="nome do evento">
                        </div>

                        <div class="form-group col-md-6">
                            <label>Data do Inicio</label>
                            <input type="datetime-local" class="form-control" name="data-inicio" id="data-inicio" value="<?= date('Y-m-d\TH:i', strtotime($this->data('lembreteModal')['inicio'])); ?>" placeholder="data">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Data do Fim</label>
                            <input type="datetime-local" class="form-control" name="data-fim" id="data-fim" value="<?= date('Y-m-d\TH:i', strtotime($this->data('lembreteModal')['fim'])); ?>">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="">Descriçao</label>
                            <input type="text" class="form-control" name="desc-event" id="desc-event" value="<?= $this->data('lembreteModal')['descricao'] ?>" placeholder="Descricao do evento">
                        </div>

                    </div>
                    <?php } ?>
                    <br>
                    <button type="button" data-dismiss="modal" class="btn btn-danger">Cancelar</button>
                    <button type="submit" id="btn-editar-evento" class="btn btn-primary">Adicionar</button>
                </form>
            </div>
        </div>
    </div>
</div>