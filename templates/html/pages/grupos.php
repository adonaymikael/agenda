<?php
include "../templates/html/pages/criarGrupo.php";
include "../templates/html/pages/grupo-membros.php";
include "../templates/html/pages/configuracoes-grupo.php";

$solicitacoes = $this->data('solicitacao_grupo'); 
?>
<div class="modal-body">
	<div class="container">
		<div class="span9 offset1">
			<button type="button" class="btn btn-primary btn" aria-pressed="false" autocomplete="off" data-toggle="modal" data-target="#criarGrupo">Criar Grupo</button>
		</div>
		<br>
		<?php if($solicitacoes > 0) {?>
		<table class="table">
			<thead>
				<tr>
					<th scope="col">Nome</th>
					<th scope="col">Solicitação</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($this->data('solicitacao_grupo') as $grupo) { ?>
					<tr>
						<td><?= $grupo['nomeGrupo'] ?></td>
						<td><a href="/aceitar-solicitacao-grupo?id_de=<?= $grupo['id_de'] ?>&gruposID=<?= $grupo['grupos_id'] ?>" class="btn btn-success">Aceitar Solicitação</a></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php } ?>
	</div>
	<br><br>
	<div class="modal-body">
		<div class="container">
			<div class="span9 offset1">
				<h3>Meus Grupos</h3>
			</div>
			<br>
			<table class="table">
				<thead>
					<tr>
						<th scope="col">Nome</th>
						<th scope="col">Operacoes</th>
						<th scope="col">Agenda</th>
						<th scope="col">Configurações</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($this->data('lista_grupos') as $grupo) { ?>
						<tr>
							<td><?= $grupo['nomeGrupo'] ?></td>
							<td><a href="carrega-membros?idGrupo/<?= $grupo['id'] ?>" class="btn btn-info membro-btn" id="membro-btn">Membros</a></td>
							<td><a href="agenda-grupo?idGrupo/<?= $grupo['id'] ?>" class="btn btn-success">Agenda do Grupo&nbsp;&nbsp;<img src="\css\svg\calendar.svg" alt="some text" width=20 height=25></a></td>
							<?php if ($grupo['id_dono'] == $_SESSION['id']) { ?>
                           		<td><a href="configuracoes-grupo?idGrupo/<?= $grupo['id'] ?>" class="btn btn-warning" id="config">Configurações</a></td>
							<?php   }  else{?>
								<td>Sem permissão</td>	
							<?php } ?>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	var url = window.location.href;
	var grupoID = url.substring(url.indexOf("grupo_id=") + 9);

	if (url.match(/grupo_id=/)) {
		$(document).ready(function() {
			$('#grupo-membros').modal('show');
		});
	}
	if (url.match(/config_id=/)) {
		$(document).ready(function() {
			$('#configuracoes-grupo').modal('show');
		});
	}
</script>