<?php
if (isset($_SESSION['erro'])) {
    $erro = $_SESSION['erro'];
} else {
    $erro = null;
}

if ($erro == "login") { ?>
    <script>
        $(document).ready(function(){
            $("#loginmodal").modal();
        });
    </script>
<?php  } ?>
<div class="modal fade" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="loginmodal" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
      <div class="modal-header">
         <h5 class="modal-title" >Login</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
      <div class="modal-body">
         <div class="container">
            <div class="span9 offset1">
               <form name="login" action="/acesso" method="POST">
                  <center>
                     <?php if ($erro == "login") { ?>
                     <div class="alert alert-danger" role="alert">
                        <b>Email</b> ou <b>Senha</b> incorretos. Por favor, tente novamente
                     </div>
                     <?php 
                } ?>
                     <img src="\css\svg\person.svg" alt="some text" width=80 height=100> </a><br>    
                     <h1>Login</h1>
                     <br>
                     <div class="form-group col-md-6">
                     <input required class="form-control" type="text" name="email" id="email" placeholder="Digite seu Email"><br><br><br>
                     <input required class="form-control" type="password" name="senha" id="senha" placeholder="Digite sua senha"><br><br>
                     </div>
                     <input type="button" class="btn btn-secondary" data-dismiss="modal" value="Voltar">&emsp;&emsp;&emsp;<input type="submit"  class="btn btn-primary" value="Entrar"><br><br><br>
                     
                  </center>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>  