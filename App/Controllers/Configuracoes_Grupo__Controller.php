<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Configuracoes_Grupo__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        $id = $_SESSION['id'];
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            \App\Operacoes::editar_grupo($id);
            header('location:/grupos');
            exit();
        }
        $idGrupo = Input::args('idGrupo');
        $id = $_SESSION['id'];
        header('location:grupos?id='.$id."&config_id=".$idGrupo);
        $config_id = Input::args('config_id');
        $this->set( 'listarMembros', \App\Operacoes::lista_membros($config_id));
        
    }
}