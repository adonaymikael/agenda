<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-eniz{font-weight:bold;border-color:#ffffff;text-align:center}
.tg .tg-s6z2{border-color:#ffffff;text-align:center}
</style>

<div class="modal fade" id="adicionarmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adicionar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="mb-5">Adicionar os dados</h3>

<form action="/adicionar-evento-grupo" method="POST">

<div class="form-group ">
<table class="tg" >
  <tr>
    <th class="tg-eniz">Cor:&emsp;<input type="color" class="form-event" name="cor" id="cor-evento" placeholder="cor">&emsp;&emsp;</th>
  </tr>
</table>    
        <br>
    </div>

    <div class="form-row">
        
        <div class="form-group col-md-6">
            <label>Nome do Evento</label>
            <input type="text" class="form-control" name="title" id="nome-evento" placeholder="nome do evento">
        </div>
        
        <div class="form-group col-md-6">
            <label>Data do Inicio</label>
            <input type="datetime-local" class="form-control" name="data-inicio" id="startEvent" value="startEvent" placeholder="data"> 
        </div>
        
    </div>
    <div class="form-row">
    <div class="form-group col-md-6">
        <label>Data do Fim</label>
        <input type="datetime-local" class="form-control" name="data-fim" id="endEvent" value="endEvent" placeholder="data">
    </div>
    
    <div class="form-group col-md-6">
        <label for="">Descriçao</label>
        <input type="text" class="form-control" name="desc-event" id="desc-event" placeholder="Descricao do evento">
    </div>

    </div>
    <button type="submit" class="btn btn-danger">Cancelar</button> 
    <button type="submit" class="btn btn-primary">Adicionar</button>
</form>
</div>
</div>
</div>
</div>