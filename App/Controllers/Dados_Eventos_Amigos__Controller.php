<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para para servidor pesquisado por nome.
 */
class Dados_Eventos_Amigos__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {

        $this->format = 'json';

        $data     = [];
        $idAmigo = Input::args('id_amigo');
        $data = \App\Operacoes::eventosAmigos($idAmigo);
        return $data;
    }
}