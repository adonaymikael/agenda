<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para para servidor pesquisado por nome.
 */
class Lista_De_Usuarios__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->set('usuarios', \App\Operacoes::lista_usuarios());
            // exit();
        } 
    }
}
