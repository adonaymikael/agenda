var data = new Date();
if (data.getMonth() < 10) {
    mes = "0" + (data.getMonth() + 1);
} else {
    mes = (data.getMonth() + 1);
}
if (data.getDate() < 10) {
    dia = "0" + data.getDate();
} else {
    dia = data.getDate();
}
var dataHoje = data.getFullYear() + "-" + mes + "-" + dia;

document.addEventListener('DOMContentLoaded', function () {
    var calendarAmigo = document.getElementById('calendarAmigo');

    var calendar = new FullCalendar.Calendar(calendarAmigo, {
        locale: 'pt-br',
        plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        defaultDate: dataHoje,
        navLinks: true,
        selectable: true,
        selectMirror: true,
        eventRender: function (info) {
            var tooltip = new Tooltip(info.el, {
                title: info.event.extendedProps.description,
                placement: 'top',
                trigger: 'hover',
                container: 'body'
            });
        },
        events: "/dados-eventos-amigos",
        eventLimit: true,
    });

    calendar.render();
});
