<?php
if (isset($_GET['grupo_id'])) {
   $id = $_GET['grupo_id'];
} else {
   $id = null;
}
$membros = $this->data('listarMembros');
?>
<div class="modal fade" id="grupo-membros" tabindex="-1" role="dialog" aria-labelledby="grupo-membros" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Membros</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="container">
               <div class="span9 offset1">
               <center>
                  <?php if($this->data('listarMembros') > 0){ ?>
                     <h1>Grupo <font color=blue><?= $membros[0]["nomeGrupo"] ?></font></h1>
                     <br>
                     <div class="form-group col-md-6">
                        Membros
                        <table class="table">
                           <tbody>
                              <?php  foreach ($this->data('listarMembros') as $membros) { ?>
                              <tr>
                                 <td><b><?= $membros['nome'] ?></b></td>
                              </tr>
                              <?php } ?>
                           </tbody>
                        </table>
                     </div>
                     <br>
                  <?php   } ?>
                  <?php if($this->data('listarMembrosSolicitacao') > 0){ ?>
                     <div class="form-group col-md-6">
                        Com solicitação
                        <table class="table">
                           <tbody>
                              <?php  foreach ($this->data('listarMembrosSolicitacao') as $membrosPendente) { ?>
                              <tr>
                                 <td><?= $membrosPendente['nome'] ?></td>
                              </tr>
                              <?php } ?>
                           </tbody>
                        </table>
                     </div>
                     <input type="button" class="btn btn-secondary" data-dismiss="modal" value="Fechar">
                  <?php   } ?>
                 </center>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>