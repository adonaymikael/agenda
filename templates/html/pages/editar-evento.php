<div class="modal fade" id="editarmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="mb-5">Editar os dados</h3>

<form action="/editar-evento" method="POST">
<div class="form-group ">   
<table class="tg" >
  <tr>
    <th class="tg-eniz">Cor:&emsp;<input type="color" class="form-event" name="cor-editar" id="corEvent" value="corEvent" placeholder="cor">&emsp;&emsp;</th>
    <th class="tg-s6z2"><div class="double"><input type="checkbox" value="1" name="share" checked></div></th>
    <th class="tg-eniz">Compartilhar com amigos</th>
  </tr>
</table>
</div>

    <div class="form-row">
        <div class="form-group col-md-6">
        <input type="text" class="form-control" name="eventid" id="eventid" value="eventid" hidden>
            <label>Nome do Evento</label>
            <input type="text" class="form-control" name="title-editar" id="tituloEvent" value="tituloEvent" placeholder="nome do evento">
        </div>
        
        <div class="form-group col-md-6">
            <label>Data do Inicio</label>
            <input type="datetime-local" class="form-control" name="data-inicio-editar" id="startEvent" value="startEvent" placeholder="data"> 
        </div>
        
    </div>
    <div class="form-row">
    <div class="form-group col-md-6">
        <label>Data do Fim</label>
        <input type="datetime-local" class="form-control" name="data-fim-editar" id="endEvent" value="endEvent" placeholder="data">
    </div>
    
    <div class="form-group col-md-6">
        <label for="">Descriçao</label>
        <input type="text" class="form-control" name="desc-event" id="descricaoEvent" value="descricaoEvent" placeholder="Descricao do evento">
    </div>

    </div>
    <br>
    <input type="button" class="btn btn-danger" onclick="deletarEvento(eventid.value);" value="Excluir">&emsp;
    <button type="submit" id="btn-editar-evento" class="btn btn-primary">Editar</button>
</form>
</div>
</div>
</div>
</div>

<script>

    function deletarEvento(id){
        window.location.href = "/excluir_evento?eventid="+id;
    }
</script>
