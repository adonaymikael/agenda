<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Acesso__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            \App\Operacoes::login();
            header("location:calendario?id/".$_SESSION['id']);
            exit();
        }   
    }
}