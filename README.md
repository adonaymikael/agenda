## Link para acessar o projeto.
[Projetoagenda.herokuapp](https://projetoagenda.herokuapp.com/)

### Imagem do projeto atual:
![](/prints/img1.png)

# **Instruções de linha de comando:**


### **Configuração global do Git**
```
git config --global user.name "USER"
git config --global user.email "USER@EMIAL.com"
```


### **Crie um novo repositório**
```
git init
git remote add origin https://gitlab.com/NOME/NOME
cd existing_folder
git add .
git commit -m "Initial commit"
git push -u origin master
```

### **Pasta existente**
```
cd existing_folder
git remote rename origin old-origin
git remote add origin https://gitlab.com/NOME/NOME
git add .
git commit -m "Initial commit"
git push -u origin master
```

### **Repositório Git existente**
```
git clone https://gitlab.com/NOME/NOME
cd existing_folder
git add .
git commit -m "Initial commit"
git push -u origin master
```

### **Baixando alterações em origin**
```
git pull
```

### **Executando projeto em localhost**
```
php -S localhost:4040 -t public
```

### **Heroku**
```
heroku login
heroku git:remote -a NOME
git add .
git commit -am "make it better"
git push heroku master
```

### **Log de erros do Heroku**
```
heroku logs -n 1500
```