<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Excluir_do_Grupo__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        $id = Input::args('id');
        $id_grupo = Input::args('id-grupo');
        \App\Operacoes::Excluir_Do_Grupo($id, $id_grupo);
        header('location:grupos');
    }
}