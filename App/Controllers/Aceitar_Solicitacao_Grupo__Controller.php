<?php

namespace App\Controllers;

use Core\HTTP\Controller;
use Core\HTTP\Input;

/**
 * Controlador para alterar servidor.
 */
class Aceitar_Solicitacao_Grupo__Controller extends Controller
{
    /**
     * Implementa o serviço.
     */
    public function execute()
    {
        $id_de   = $_GET['id_de'];
        $grupos_id   = $_GET['gruposID'];
        $id_para = $_SESSION['id'];
        \App\Operacoes::aceita_solicitacao_grupo($id_de, $id_para,$grupos_id);
        header('location:grupos');
        
    }
}